#!/usr/bin/env python

'''
a simple script that takes two csv files from twitter's #rbnews stream, finds their common columns and extracts
the rows for each column from each file and writes it to a new file, while removing any duplicates based on the id_str key 

version:2.0
'''

import csv
import StringIO
import sys
import time

def get_common_columns():
    file1 = sys.argv[1]
    file2 = sys.argv[2]
    output_file = sys.argv[3] + ".csv"

    with open(file1,'rb') as csvfile:
        tw1csv = csv.reader(csvfile, delimiter='\t')
        list1 = list(tw1csv)
        list1_cols = list1[0]
        
    with open(file2,'rb') as csvfile:
        tw2csv = csv.reader(csvfile, delimiter='\t')
        list2 = list(tw2csv)
        list2_cols = list2[0]

    print "file1 columns:" + str(list1_cols) + '\n'
    print "file2 columns:" + str(list2_cols) + '\n'

    index_of_tweet_id_for_file1 = list1_cols.index('id_str')
    print "index of tweet id:",index_of_tweet_id_for_file1
    index_of_tweet_id_for_file2 = list2_cols.index('id_str')
    print "index of tweet id:",index_of_tweet_id_for_file2


    #find all common columns and get their column index in each file. make a dict of {index:column} for each file
    all_columns = []
    file1_dict = {}
    file2_dict = {}

    for col_obj in list1_cols:
        if (col_obj in list2_cols):
            all_columns.append(col_obj)
            tw1index = list1_cols.index(col_obj) #get index of column object for file 1
            file1_dict.update({tw1index:col_obj}) # add to dict

            tw2index = list2_cols.index(col_obj) #get index of column object for file 2
            file2_dict.update({tw2index:col_obj}) #add to dict

    print 'Common Columns ' + str(all_columns) + '\n'
    print "dict1:" + str(file1_dict) + '\n'
    print "dict2:" + str(file2_dict) + '\n'

    #now for each file use the indexes to extract only the necessary columns
   
    file1_keys = file1_dict.keys() #file1 keys (i.e indexes)
    file2_keys = file2_dict.keys() #file2 keys


    csvfile1_in =  open(file1,'rb')
    csvreader1 = csv.reader(csvfile1_in, delimiter='\t')

    csvfile2_in =  open(file2,'rb')
    csvreader2 = csv.reader(csvfile2_in, delimiter='\t')
    
    # merged_file = '/Users/mpat-man/Documents/rbnews_twitter_analysis/merging/merged2.csv'
    merged_file = output_file
    csvfile_out = open(merged_file,'wb')
    csvwriter = csv.writer(csvfile_out, delimiter='\t')

    added_ids = []
    duplicateIds = []
    file1_rows = 0;
    file1_dups = 0
 
    indexOfRow = 0
    file1_errors = 0
    file1_errors_list = []



    for row in csvreader1:        
        print "==== NEW ROW FOR " + file1 + " "  + str(indexOfRow) + " ==== \n"        
        row_out = []
        # index_of_tweet_id = row.index('id_str')
        # print "*****INDEX OF TWEET ID******:",index_of_tweet_id
        id_str = row[index_of_tweet_id_for_file1]

        for key in file1_keys:

            try:
                print file1_dict[key] + ' ->  ' + row[key] + '\n'
                row_out.append(row[key])
                # print "ROW OUT:",row_out
            except:
                if (not id_str in file1_errors_list):
                    print "Error " + str(TypeError)
                    row_out.append("Error Value")
                    file1_errors+=1
                    file1_errors_list.append(id_str) #add the id to the errors list

        if (not id_str in added_ids):
            # added_ids.update({row[0]:""})
            csvwriter.writerow(row_out)
            file1_rows+=1
            added_ids.append(id_str)
        else:
            file1_dups+=1
            duplicateIds.append(id_str)

        indexOfRow+=1
       

                         
    file2_rows = 0
    file2_dups = 0

    indexOfRows = 0
    file2_errors = 0
    file2_errors_list = []

    for row in csvreader2:
        print "==== NEW ROW FOR " + file2 + " "  + str(indexOfRow) + " ==== \n"        
        row_out = []
        id_str = row[index_of_tweet_id_for_file2]

        for key in file2_keys:
            try:
                field_name = file2_dict[key]
                print field_name + ' ->  ' + row[key] + '\n'

                #get the column index in the first file for this field name
                index_of_field_in_first_file = file1_dict.values().index(field_name)

                # print "index of " + field_name + " in file1:", index_of_field_in_first_file

                #insert the data at the correct index
                row_out.insert(index_of_field_in_first_file, row[key])
            except:
                if (not id_str in file2_errors_list):
                    print "Error " + str(TypeError)
                    row_out.append("Error Value")
                    file2_errors+=1
                    file2_errors_list.append(id_str) #add the id to the errors list
                
        if (not id_str in added_ids):
            csvwriter.writerow(row_out)
            added_ids.append(id_str)
            file2_rows+=1
        else:
            # print "duplicate :",row[0]
            file2_dups+=1
            duplicateIds.append(id_str)

        indexOfRow+=1

    print " ========= RESULTS SUMMARY ========= \n"
    # print "all tweet IDs :", added_ids
    # print "duplicate tweet IDs from " + file1 + ": " + str(file1_dups)
    # print "duplicate tweet IDs from " + file2 + ": " + str(file2_dups)
    # print "all duplicate IDs:", duplicateIds
    print "added rows from " + file1 + ": " + str(file1_rows)
    print "duplicates from " + file1 + ": " + str(file1_dups)
    print "errors from " + file1 + ": " + str(file1_errors)
    print "errors list from " + file1 + ": " + str(file1_errors_list)
    print "\n"
    print "added rows from  " + file2 + ": " + str(file2_rows)
    print "duplicates from " + file2  + ": " + str(file2_dups)
    print "errors from " + file2 + ": " + str(file2_errors)
    print "errors list from " + file2 + ": " + str(file2_errors_list)
    print "\n"
#    print "total keys:",len(added_ids)
    print "total rows added: ", file1_rows + file2_rows
    print "total duplicates: ",len(duplicateIds)

    
        

if __name__ == "__main__":
    get_common_columns()
    # get_columns()
